<div class="head">
<!--
  ![image info](images/Header.png)
 
  <img height="180em" width="350px" src="https://gitlab.com/alliliumm/alliliumm/-/blob/main/images/Header.png">

  <br><br>
  
  <div align="center">
    <img height="80" width="80" src="https://gitlab.com/alliliumm/alliliumm/-/blob/main/loading-done_2_.gif">
  </div>
  -->
</div>

<div class="body">
  <div class="header">
  <div class="start-text">
    <div>
      <h2 align="center" style="font-family:Times" >Sobre mim</h2>
      <p>O primeiro contato que obtive com a programação foi no curso técnico de informática, o qual me deu os primeiros passos para seguir nessa área, assim determinei os meus objetivos e minhas metas para evoluir cada vez mais, não somente como programadora, mas também como pessoa e aprendiz por toda a vida. <br><br>
      Contudo, busco desafios e aventuras nessa área para entregar soluções pensadas para cada obstáculo que houver, e com isso, poder aprender mais e ainda me divertir durante o percurso da evolução.&#128512;
      </p>
    </div>
  </div>
  </div>


  <div class="main">

  <div>
    <h2 align="center" style="font-family:Times" >Estatísticas</h2>
  </div>

  <div class="git-status" align="center">
    <a href="https://github.com/alliliumm">
    <img height="180em" src="https://github-readme-stats.vercel.app/api?username=alliliumm&show_icons=true&theme=dracula&include_all_commits=true&count_private=true"/>
    <img height="180em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=alliliumm&layout=compact&langs_count=7&theme=dracula"/>
    </a>
  </div>

  <div class="git-wakatime" align="center">
    <a href="https://wakatime.com/@alliliumm">
    <img height="180em" width="350px" src="https://github-readme-stats.vercel.app/api/wakatime?username=alliliumm&theme=dracula"/>
    </a>
  </div>

  <div>
    <h2 align="center" style="font-family:Times" >Habilidades</h2>
  </div>

  <div class="git-languages" align="center" style="display: inline_block"><br>
    <img align="center" alt="JavaScript" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-plain.svg">
    <img align="center" alt="Type-Script" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/typescript/typescript-plain.svg">
    <img align="center" alt="HTML" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original.svg">
    <img align="center" alt="CSS" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original.svg">
    <img align="center" alt="PHP" height="60" width="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/php/php-plain.svg" />
  </div>

  <br>

  <div class="git-frameworks" align="center" style="display: inline_block">
    <img align="center" alt="Flutter" height="30" width="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/flutter/flutter-original.svg" />
    <img align="center" alt="Ionic" height="30" width="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/ionic/ionic-original.svg" />
    <img align="center" alt="VueJS" height="30" width="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/vuejs/vuejs-original.svg" />
  </div>

  <br>

  <div align="center" style="display: inline_block">
    <img align="center" alt="MySQL" height="60" width="70" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/mysql/mysql-plain-wordmark.svg" />
    <img align="center" alt="Firebase" height="50" width="70" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/firebase/firebase-plain-wordmark.svg" />
    <img valign="center" alt="OracleSQL" height="55" width="55" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/oracle/oracle-original.svg" />
  </div>

  <br>

  <div>
    <h2 align="center" style="font-family:Times" >Projetos Principais</h2>
  </div> 

  <div class="git-repos" align="center">
  <a href="https://github.com/alliliumm/Adminio-APP">
    <img align="center" src="https://github-readme-stats.vercel.app/api/pin/?username=alliliumm&amp;repo=Adminio-APP&amp;theme=dracula" style="max-width: 100%;">
  </a>
  </div>

  <br>
    <div>
    <h2 align="center" style="font-family:Times" >Estudos</h2>
  </div> 

  <div class="git-repos" align="center">
  <a href="https://github.com/alliliumm/Projetos-Estudos">
    <img src="https://github-readme-stats.vercel.app/api/pin/?username=alliliumm&amp;repo=Projetos-Estudos&amp;theme=dracula" style="max-width: 100%;">
  </a>
  <a href="https://github.com/alliliumm/Estudos">
    <img src="https://github-readme-stats.vercel.app/api/pin/?username=alliliumm&amp;repo=Estudos&amp;theme=dracula" style="max-width: 100%;">
  </a>
  </div>


  </div>

##

  <div class="footer">
  <!--
    <div>
      <img align="right" alt="Ale-pic" height="150" src="https://gitlab.com/alliliumm/alliliumm/-/blob/main/perf-v1.png">
    </div>
  -->
    <div class="end-text">
      <p>Foi um prazer ter você aqui!!<br> 
      Agradeço pelo seu breve momento pelo meu perfil. &#128522;<br>
      Se quiser, dê um passeio pelas minhas redes:
      </p>
      <div class="header-socials">
        <a href="https://www.linkedin.com/in/alessandra-teles911" target="_blank"><img src="https://img.shields.io/badge/-LinkedIn-%230077B5?style=for-the-badge&logo=linkedin&logoColor=white" target="_blank"></a> 
        <a href="https://gitlab.com/alessandrateles911" target="_blank"><img src="https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white" target="_blank"></a> 
        <a href="mailto:alessandrateles911@gmail.com"><img src="https://img.shields.io/badge/Gmail-D14836?style=for-the-badge&logo=gmail&logoColor=white" target="_blank"></a>
      </div>
    </div>
  </div>

  <!--![Snake animation](https://github.com/alliliumm/alliliumm/blob/output/github-contribution-grid-snake.svg)-->
</div>



